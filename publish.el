(require 'org)
(require 'htmlize)
(require 'ox-publish)
(require 'org-ref)

(setq org-html-htmlize-output-type 'css)

(setq python-indent-guess-indent-offset t)
(setq python-indent-guess-indent-offset-verbose nil)

(setq org-export-babel-evaluate nil)

(add-to-list 'org-src-lang-modes '("python" . python))
(setq org-export-with-todo-keywords nil)

(add-to-list 'load-path "./include/compose-publish")
(require 'compose-publish)
